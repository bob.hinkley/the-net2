package internetTestPackage;

import Base.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class po_InternetHomepage extends BaseTest {
    @Test
    public void verifyInternetHomepage(){
        Assert.assertEquals(driver.getTitle(),"The Internet");
    }

    @Test
    public void verifyInternetHomepageTitle(){
        Assert.assertEquals(driver.findElement(By.xpath("//h1[text()='Welcome to the-internet']")).isDisplayed(), true);
    }

    @Test
    public void verifyInternetHomepageTitlee(){
        Assert.assertEquals(driver.findElement(By.xpath("//h1[text()='Welcome to the-internet']")).isDisplayed(), true);
    }
}

